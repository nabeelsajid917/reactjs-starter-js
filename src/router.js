import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { useSelector } from 'react-redux';
// import {isLoggedIn } from '../redux/auth/selectors';
const Routing = () => {
    // const loginStatus = useSelector(isLoggedIn)
    var loginStatus = false;
    return(
      <Router>
        {loginStatus ? 
        <Switch>
          {/* <Route path="/" exact component={Dashboard} /> */}
        </Switch>
        : 
        <Switch>
            <Route path="/login" render = {()=> <Redirect to="/login" />} />
        </Switch>
        }
      </Router>
    )
  }

  export default Routing
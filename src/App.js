import "./app.css";
import { Provider } from 'react-redux';
import { store } from "./redux/store";
import  Routing from "./router";
const App = () => {
  return (
    <div className="App">
      <Provider store={store}>
          <Routing />
          <h1>
            Hello world!
          </h1>
      </Provider>
    </div>
  );
};

export default App;

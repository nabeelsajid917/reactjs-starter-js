import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware ,{ END } from "redux-saga";
import rootReducer from "./root-reducer";
import rootSaga from "./root-saga";
import storage from "redux-persist/lib/storage";
import {persistReducer} from "redux-persist";

const sagaMiddleWare = createSagaMiddleware()
const bindMiddleware = middleware => {
    if(process.env.NODE_ENV !== "production"){
        const { composeWithDevTools } = require ("redux-devtools-extension");
        return composeWithDevTools(applyMiddleware(...middleware));
    }
    return compose(applyMiddleware(...middleware))
} 
const initial = window.__INITIAL_STATE__;
function configureStore(initialState = initial){
    let store;
    const isClient = typeof window !== "undefined"
    if(isClient){
        const persistConfig = {
            key: "root",
            whiteList: ["authentication"],
            storage
        };
        const persistedReducer = persistReducer(persistConfig, rootReducer);
        store = createStore(
            persistedReducer,
            initialState,
            bindMiddleware([sagaMiddleWare])
        );
    }
    else {
        store = createStore(
            rootReducer,
            initialState,
            bindMiddleware([sagaMiddleWare])
        )
    }
    store.runSaga = () => {
        // Avoid running twice
        if (store.saga) return;
        store.saga = sagaMiddleWare.run(rootSaga);
      };
    
      store.stopSaga = async () => {
        // Avoid running twice
        if (!store.saga) return;
        store.dispatch(END);
        await store.saga.done;
        store.saga = null;
      };
    
      store.execSagaTasks = async (isServer, tasks) => {
        // run saga
        store.runSaga();
        // dispatch saga tasks
        tasks(store.dispatch);
        // Stop running and wait for the tasks to be done
        await store.stopSaga();
        // Re-run on client side
        if (!isServer) {
          store.runSaga();
        }
      };
    
      // Initial run
      store.runSaga();

      return store;
}

export const store = configureStore();